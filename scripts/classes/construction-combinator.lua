return {
	on_place = function(entity)
		conn = entity.surface.create_entity {
			name = "construction-combinator-logistic-connection",
			position = entity.position,
			force = entity.force
		} -- create the logistic connection chest
		conn.destructible = false

		object = {
			entity = entity,
			logistic_connection = conn,
			item_counts = {},
			logistic_network = nil
		}

		object.combinator_params = {
			parameters = {}
		}

		return object
	end,

	on_destroy = function(object)
		object.logistic_connection.destroy()
	end,

	on_pre_update = function(object)
		conn = object.logistic_connection
		if conn then
			object.logistic_network = object.logistic_connection.logistic_network
		else
			object.logistic_network = nil
		end

		if not object.item_counts then
			object.item_counts = {}
		end
	end,

	on_ghost_added = function(object, ghost, ghost_item_counts)
		for k, v in pairs(ghost_item_counts) do
			object.item_counts[k] = (object.item_counts[k] or 0) + v
		end

		if debug_update then
			game.print("Added")
		end
	end,

	on_ghost_removed = function(object, ghost, ghost_item_counts)
		for k, v in pairs(ghost_item_counts) do
			object.item_counts[k] = (object.item_counts[k] or 0) - v
		end

		if debug_update then
			game.print("Removed")
		end
	end,

	on_ghosts_cleared = function(object)
		object.item_counts = {}

		if debug_update then
			game.print("Cleaned")
		end
	end,

	on_finalise_update = function(object)
		object.signal_table = build_signal_table(object.item_counts)

		object.combinator_params = {
			parameters = object.signal_table
		}
	end,

	on_tick = function(object)
		local control = object.entity.get_control_behavior()

		if control then
			if object.combinator_params then
				control.parameters = object.combinator_params
			else
				control.parameters = { parameters = {} }
			end
		end
	end
}
