data:extend({
  {
    type = "constant-combinator",
    name = "construction-combinator",
    icon = "__base__/graphics/icons/constant-combinator.png",
    flags = {"placeable-neutral", "player-creation"},
    minable = {hardness = 0.2, mining_time = 0.5, result = "constant-combinator"},
    max_health = 120,
    corpse = "small-remnants",
    icon_size = 32,

    collision_box = {{-0.35, -0.35}, {0.35, 0.35}},
    selection_box = {{-0.5, -0.5}, {0.5, 0.5}},

    item_slot_count = 200,

    sprites =
    {
      north =
      {
        filename = "__ConstructionCombinator__/graphics/entity/combinator/construction-combinator.png",
        x = 158,
        width = 79,
        height = 63,
        frame_count = 1,
        shift = {0.140625, 0.140625},
      },
      east =
      {
        filename = "__ConstructionCombinator__/graphics/entity/combinator/construction-combinator.png",
        width = 79,
        height = 63,
        frame_count = 1,
        shift = {0.140625, 0.140625},
      },
      south =
      {
        filename = "__ConstructionCombinator__/graphics/entity/combinator/construction-combinator.png",
        x = 237,
        width = 79,
        height = 63,
        frame_count = 1,
        shift = {0.140625, 0.140625},
      },
      west =
      {
        filename = "__ConstructionCombinator__/graphics/entity/combinator/construction-combinator.png",
        x = 79,
        width = 79,
        height = 63,
        frame_count = 1,
        shift = {0.140625, 0.140625},
      }
    },

    activity_led_sprites =
    {
      north =
      {
        filename = "__base__/graphics/entity/combinator/activity-leds/arithmetic-combinator-LED-N.png",
        width = 11,
        height = 10,
        frame_count = 1,
        shift = {0.296875, -0.40625},
      },
      east =
      {
        filename = "__base__/graphics/entity/combinator/activity-leds/arithmetic-combinator-LED-E.png",
        width = 14,
        height = 12,
        frame_count = 1,
        shift = {0.25, -0.03125},
      },
      south =
      {
        filename = "__base__/graphics/entity/combinator/activity-leds/arithmetic-combinator-LED-S.png",
        width = 11,
        height = 11,
        frame_count = 1,
        shift = {-0.296875, -0.078125},
      },
      west =
      {
        filename = "__base__/graphics/entity/combinator/activity-leds/arithmetic-combinator-LED-W.png",
        width = 12,
        height = 12,
        frame_count = 1,
        shift = {-0.21875, -0.46875},
      }
    },

    activity_led_light =
    {
      intensity = 0.8,
      size = 1,
      color = {r = 1.0, g = 1.0, b = 1.0}
    },

    activity_led_light_offsets =
    {
      {0.296875, -0.40625},
      {0.25, -0.03125},
      {-0.296875, -0.078125},
      {-0.21875, -0.46875}
    },

	circuit_wire_connection_points =
    {
      {
        shadow =
        {
          red = {0.15625, -0.28125},
          green = {0.65625, -0.25}
        },
        wire =
        {
          red = {-0.28125, -0.5625},
          green = {0.21875, -0.5625},
        }
      },
      {
        shadow =
        {
          red = {0.75, -0.15625},
          green = {0.75, 0.25},
        },
        wire =
        {
          red = {0.46875, -0.5},
          green = {0.46875, -0.09375},
        }
      },
      {
        shadow =
        {
          red = {0.75, 0.5625},
          green = {0.21875, 0.5625}
        },
        wire =
        {
          red = {0.28125, 0.15625},
          green = {-0.21875, 0.15625}
        }
      },
      {
        shadow =
        {
          red = {-0.03125, 0.28125},
          green = {-0.03125, -0.125},
        },
        wire =
        {
          red = {-0.46875, 0},
          green = {-0.46875, -0.40625},
        }
      }
    },

    circuit_wire_max_distance = 9
  },
	
})

local entity =
	{
		type = "active-provider",
		name = "construction-combinator-logistic-connection",
		item_slot_count = 1000,
		hidden = true,
	}
	
te = util.table.deepcopy(data.raw["logistic-container"]["logistic-chest-active-provider"])
te.name = entity.name
te.minable.result = entity.mine_res
te.iventory_size = entity.item_slot_count
te.order = "?"
te.collision_mask = {}
te.selection_box = {{0, 0}, {0, 0}}
--te.picture.filename = "__crafting_combinator__/graphics/trans.png"
te.picture.width = 1
te.picture.height = 1

data:extend{te, nil, nil}

	-- local ti
	-- local tr
	-- if not e.hidden then
		-- -- item
		-- ti = util.table.deepcopy(data.raw["item"]["constant-combinator"])
		-- ti.name = e.name
		-- ti.place_result = e.name
		-- ti.order = "b[combinators]-cb[construction-combinator]"
		
		-- -- recipe
		-- tr = util.table.deepcopy(data.raw["recipe"]["constant-combinator"])
		-- tr.name = e.name
		-- tr.result = e.name
	-- end
	
	-- -- add to data
	-- data:extend{te, ti, tr}
	
-- -- here we create our entities with all the necessary stuff
-- for _, e in ipairs(entities) do
	-- -- entity
	-- local te
	-- if not e.type or e.type == "constant-combinator" then -- default type to constant-combinator
		-- te = util.table.deepcopy(data.raw["constant-combinator"]["constant-combinator"])
		-- te.name = e.name
		-- te.minable.result = e.name
		-- te.item_slot_count = e.item_slot_count
		-- te.sprites = e.sprites
		
	-- elseif e.type == "active-provider" then
		-- te = util.table.deepcopy(data.raw["logistic-container"]["logistic-chest-active-provider"])
		-- te.name = e.name
		-- te.minable.result = e.mine_res
		-- te.iventory_size = e.item_slot_count
		
		-- if e.hidden then
			-- te.order = "?"
			-- te.collision_mask = {}
			-- te.selection_box = {{0, 0}, {0, 0}}
			-- --te.picture.filename = "__crafting_combinator__/graphics/trans.png"
			-- te.picture.width = 1
			-- te.picture.height = 1
		-- end
	-- end
	
	-- local ti
	-- local tr
	-- if not e.hidden then
		-- -- item
		-- ti = util.table.deepcopy(data.raw["item"]["constant-combinator"])
		-- ti.name = e.name
		-- ti.place_result = e.name
		-- ti.order = "b[combinators]-cb[construction-combinator]"
		
		-- -- recipe
		-- tr = util.table.deepcopy(data.raw["recipe"]["constant-combinator"])
		-- tr.name = e.name
		-- tr.result = e.name
	-- end
	
	-- -- add to data
	-- data:extend{te, ti, tr}
-- end
