data:extend({
  {
    type = "technology",
    name = "construction-combinator",
    icon = "__base__/graphics/technology/circuit-network.png",
    icon_size = 64,
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "construction-combinator"
      },
    },
    prerequisites = {"circuit-network"},
    unit =
    {
      count = 250,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1}
      },
      time = 15
    },
    order = "a-d-d"
  }
})