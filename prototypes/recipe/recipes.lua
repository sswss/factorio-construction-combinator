data:extend({
  {
    type = "recipe",
    name = "construction-combinator",
    enabled = "false",
    ingredients =
    {
      {"advanced-circuit", 3},
      {"constant-combinator", 1}
    },
    result = "construction-combinator"
  },
})