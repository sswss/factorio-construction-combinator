data:extend(
{ 
  {
    type = "item",
    name = "construction-combinator",
    icon = "__ConstructionCombinator__/graphics/icons/construction-combinator-icon.png",
    flags = { "goes-to-quickbar" },
    icon_size = 32,
    subgroup = "circuit-network",
    place_result="construction-combinator",
    order = "c[combinators]-c[construction-combinator]",
    stack_size= 50,
  },
}) 