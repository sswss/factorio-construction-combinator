-- Example Train settings:
-- 
-- Supply:
-- 
--   - Circuit: Green signal > 0
--   - AND 5 seconds inactivity
-- 
-- Drop:
-- 
--   - 20 seconds inactivity
--   - OR 120 seconds passed
--   - OR Circuit: Pink signal > 0
--   - AND 5 seconds inactivity
-- 
-- 
-- Clear:
-- 
--   - Empty cargo inventory


local classes = require("scripts/classes/main")
local ghosts_pending_scan = {}
local removed_entities = {}
local dirty = false
local num_known_ghosts = 0
local multikey = require("multikey")
local known_ghosts = multikey.new()
local initialised = false
local debug_update = false

function print_ghost_entity(ghost)
	local name get_ghost_name(ghost)

	game.players[1].print(
	"{ghost-name=" .. name .. "; "
	.. "position=" .. serpent.block(ghost.position) .. "; "
	.. "items=" .. print_entity_names(ghost.ghost_prototype.items_to_place_this) .. "; "
	.. "logistic_network=" .. serpent.block(ghost.logistic_network)
	.. "}\n")
end

function print_entity_names(entities)
	str = ""
	for key, _ in pairs(entities) do
		str = str .. key .. ", "
	end
	return str
end

function find_logistic_networks_at(point)
	networks = {}

	for _, surface in pairs(game.surfaces) do

		surface_network = surface.find_logistic_network_by_position(point, game.players[1].force)
		if surface_network then
			networks[#networks + 1] = surface_network
		end
	end

	return networks
end

function find_construction_networks_at(point)
	networks = {}

	-- TODO this is this run once for every rescan of a ghost entity
	-- could it be optimised by flipping the loops?
	-- Or even just using surfaces[1]?

	for _, surface in pairs(game.surfaces) do
		surface_networks = surface.find_logistic_networks_by_construction_area(point, game.players[1].force)
		for _, sn in pairs(surface_networks) do
			networks[#networks + 1] = sn
		end
	end

	return networks
end

function tablelength(T)
	local count = 0
	for _ in pairs(T) do
		count = count + 1
	end
	return count
end

function get_combinators_in_networks(networks)
	combinators = {}
	for class_name, class in pairs(classes) do
		for i, object in pairs(data[class_name]) do

			for _, network in pairs(networks) do
				if object.logistic_network == network then
					combinators[#combinators + 1] = { object = object, class = class }
				end
			end

		end
	end

	return combinators
end

function boolstr(bool)
	if bool then
		return "true" else return "false"
	end
end

function get_ghost_item_counts(ghost)
	item_counts = {}

	local name = get_ghost_name(ghost);
	local amount = 1
	if name == "curved-rail" then
		amount = 4
	end

	if is_ghost(ghost) then
		for k, v in pairs(ghost.ghost_prototype.items_to_place_this) do
			item_counts[k] = (item_counts[k] or 0) + amount
		end
	end

	if ghost.type == "entity-ghost" then
		for k, v in pairs(ghost.item_requests) do
			item_counts[k] = (item_counts[k] or 0) + v
		end
	end

	return item_counts
end

-- Stores a ghost and returns the new value at that key
function remember_ghost(ghost, networks)
	if not ghost then
		return nil
	end

	if not ghost.valid then
		return nil
	end

	forget_ghost(ghost)

	if not networks then
		networks = find_construction_networks_at(ghost.position)
	end

	local name = get_ghost_name(ghost)
	local combinators = get_combinators_in_networks(networks)
	local value = {
		ghost = ghost,
		name = name,
		position = ghost.position,
		item_counts = get_ghost_item_counts(ghost),
		combinators = combinators,
		network_count = #networks,
		type = ghost.type,
		surface = ghost.surface.index
	}

	add_known_ghost_details(ghost, value)

	num_known_ghosts = num_known_ghosts + 1

	for _, combinator in ipairs(combinators) do
		combinator.class.on_ghost_added(combinator.object, ghost, value.item_counts)
	end

	if debug_update then
		game.print("Stored ghost " .. name)
	end

	return value
end

function rescan_ghost(ghost_details, networks)
	if not networks then
		networks = find_construction_networks_at(ghost_details.position)
	end

	local old_combinators = ghost_details.combinators
	local combinators = get_combinators_in_networks(networks)

	ghost_details.combinators = combinators
	ghost_details.network_count = #networks

	known_ghosts:putv(
	ghost_details,
	ghost_details.position.x,
	ghost_details.position.y,
	ghost_details.name
	)

	for _, combinator in ipairs(old_combinators) do
		combinator.class.on_ghost_removed(
		combinator.object,
		ghost_details.ghost,
		ghost_details.item_counts)
	end

	for _, combinator in ipairs(combinators) do
		combinator.class.on_ghost_added(
		combinator.object,
		ghost_details.ghost,
		ghost_details.item_counts)
	end
end

-- Removes a ghost from the array and returns the forgotten ghost
function forget_ghost(ghost, position)
	if not ghost then
		return nil
	end

	local ghost_details = get_known_ghost_details(ghost, position)

	if ghost_details then
		forget_ghost_from_details(ghost_details)
	end

	return ghost_details
end

function forget_ghost_from_details(ghost_details)
	known_ghosts:putv(
	nil,
	ghost_details.position.x,
	ghost_details.position.y,
	ghost_details.name
	)

	num_known_ghosts = num_known_ghosts - 1

	for _, combinator in ipairs(ghost_details.combinators) do
		combinator.class.on_ghost_removed(
		combinator.object,
		ghost_details.ghost,
		ghost_details.item_counts)
	end

	if debug_update then
		game.print("Forgot ghost " .. ghost_details.name .. ", type: " .. ghost_details.type)
	end
end

function rescan_proxies (surface)
	ghosts = surface.find_entities_filtered {
		name = "item-request-proxy",
	}

	for i, proxy in ipairs(ghosts) do
		table.insert(ghosts_pending_scan, proxy)
	end
end

function is_known_ghost(ghost)
	return get_known_ghost_details(ghost) ~= nil
end

function get_ghost_name(ghost)
	if is_ghost(ghost) then
		return ghost.ghost_name
	else
		return ghost.name
	end
end

function get_known_ghost_details(ghost, position)
	return known_ghosts:get(
	position and position.x or ghost.position.x,
	position and position.y or ghost.position.y,
	get_ghost_name(ghost)
	)
end

function add_known_ghost_details(ghost, value)
	return known_ghosts:putv(
	value,
	ghost.position.x,
	ghost.position.y,
	get_ghost_name(ghost)
	)
end

function clear_known_ghosts()
	for class_name, class in pairs(classes) do
		for _, object in pairs(data[class_name]) do
			class.on_ghosts_cleared(object)
		end
	end

	known_ghosts:clear()
	num_known_ghosts = 0
end

function update_ghosts(ghosts, networks, rescan)
	local total_ghosts = 0
	local known_ghosts_used = 0
	local invalid_ghosts = 0

	--global.ghosts[surface.name] = game.surfaces[surface.name].find_entities_filtered{name="entity-ghost"}

	total_ghosts = total_ghosts + #ghosts
	for i, ghost in ipairs(ghosts) do
		if ghost.valid then
			local ghost_details = get_known_ghost_details(ghost)
			if ghost_details then
				if rescan then
					rescan_ghost(ghost_details, networks)
				else
					known_ghosts_used = known_ghosts_used + 1
				end
			else
				remember_ghost(ghost, networks)
			end
		else
			invalid_ghosts = invalid_ghosts + 1
		end

	end

	if total_ghosts == 0 then
		-- Additional safety against inconsistencies - clear the
		-- known_ghosts if the ghosts array is empty
		clear_known_ghosts()
	end

	if debug_update then
		game.print(
		"Updated; "
		.. "known_ghosts: " .. num_known_ghosts .. "; "
		.. "known_ghosts_used: " .. known_ghosts_used .. "; "
		.. "total_ghosts: " .. total_ghosts .. "; "
		.. "invalid_ghosts:" .. invalid_ghosts)
	end
end

function purge_invalid_ghosts()
	ghosts_to_remove = {}
	for i, v in ipairs(known_ghosts:values()) do
		if not v.ghost.valid then
			ghosts_to_remove[#ghosts_to_remove + 1] = v
		end
	end

	for _, ghost_details in ipairs(ghosts_to_remove) do
		forget_ghost_from_details(ghost_details)
	end

	if debug_update then
		if #ghosts_to_remove > 0 then
			game.print("Purge removed " .. #ghosts_to_remove .. " ghosts")
		end
	end
end

function build_signal_table(item_counts)
	-- This combinator has a fixed max of 200 signals per entity
	max_signals_per_combinator = 200

	signals_left = max_signals_per_combinator
	signal_table = {}
	for k, v in pairs(item_counts) do
		if signals_left == 0 then
			-- Too many signals; bail out
			break
		end

		if v ~= 0 then
			new_index = #signal_table + 1
			signal_table[new_index] = { signal = { type = "item", name = k }, count = v, index = new_index }
			signals_left = signals_left - 1
		end

		-- game.print("signal_table[" ..  new_index .. "]=" .. serpent.block(signal_table[new_index]))
	end

	return signal_table
end

function is_nonempty(tbl)
	if not tbl then
		return false
	end
	for _, v in pairs(tbl) do
		return true
	end
	return false
end

function is_empty(tbl)
	return not is_nonempty(tbl)
end

function entity_built_by_robot(event)
	mark_dirty()
	entity_built(event)
end

function entity_built(event)
	if event.item then
		for _, tilePos in ipairs(event.tiles) do
			on_built(event.item, tilePos.position)
		end
	else
		on_built(event.created_entity)
	end
end

function on_built(entity, position)
	if classes[entity.name] ~= nil then
		local tab = data[entity.name]
		table.insert(tab, classes[entity.name].on_place(entity))
		data[entity.name] = tab
		save()
	end

	if is_ghost(entity) then
		mark_dirty()
		table.insert(ghosts_pending_scan, entity)
	else
		if entity.type == "roboport" then
			local logistic_cell = entity.logistic_cell
			if logistic_cell then
				-- Trigger a rescan as the networks containing the items have to be updated
				radius = logistic_cell.construction_radius
				pos = entity.position
				area = {
					{ pos.x - radius, pos.y - radius },
					{ pos.x + radius, pos.y + radius }
				}

				network = entity.logistic_network
				update_network_area(area, network)
			end
		else
			mark_dirty()
		end
		-- if there was a ghost at this location, forget it
		forget_ghost(entity, position)
	end
end

function is_ghost(entity)
	return entity.type == "entity-ghost" or entity.type == "tile-ghost"
end

function on_entity_removed(event)
	if event.entity then
		entity_removed(event.entity)
	elseif event.ghost then
		entity_removed(event.ghost)
	end
end

function on_entity_died(event)
	on_entity_removed(event)
	
	if event.entity and event.entity.type ~= "unit" and event.entity.type ~= "tree" then
		for __, surface in pairs(game.surfaces) do
			table.insert(removed_entities, event.entity.position)
		end
	end
end

function entity_removed(entity)
	if is_ghost(entity) then
		mark_dirty()
		forget_ghost(entity)
	end

	local name = get_ghost_name(entity);

	if classes[name] ~= nil then
		for k, v in ipairs(data[name]) do
			if v.entity == entity then
				local tab = data[event.entity.name]
				table.remove(tab, k)
				classes[event.entity.name].on_destroy(v)
				data[event.entity.name] = tab
				save()
				break
			end
		end
	end
end

function save()
	global.uc_data = data
end

function tick()
	local update_interval_secs = 1
	local update_initial_interval_secs = 5
	local update_inverse_initial_delay = update_interval_secs - update_initial_interval_secs
	if (game.tick + update_inverse_initial_delay * 60) % (update_interval_secs * 60) == 0 then
		update()
	end

	for k, v in pairs(classes) do
		for q, i in pairs(data[k]) do
			if i.entity.valid then
				v.on_tick(i, q)
			end
		end
	end
end

function update()
	if not initialised then
		-- Clear the item_counts each time we start up
		-- Note: would be better to just not store them in the first place
		for class_name, class in pairs(classes) do
			for _, object in pairs(data[class_name]) do
				class.on_ghosts_cleared(object)
			end
		end

		rescan_all()

		initialised = true
	end

	if #removed_entities > 0 then
		for i, position in ipairs(removed_entities) do
			if i > 100 then break end
			table.remove(removed_entities, i)

			for __, surface in pairs(game.surfaces) do
				local ghosts = game.surfaces[surface.name].find_entities_filtered{
					position = position,
					name = {"entity-ghost"}
				}

				for i, ghost in ipairs(ghosts) do
					on_built(ghost)
				end
			end
		end
	end

	if dirty then
		local num_ghosts_to_scan = math.min(150,#ghosts_pending_scan)

		local ghosts_to_scan = {}
		for i = 1, num_ghosts_to_scan do
			ghosts_to_scan[i] = ghosts_pending_scan[i]
		end

		local new_ghosts_pending_scan = {}
		local j = 1
		for i = #ghosts_to_scan + 1, #ghosts_pending_scan do
			new_ghosts_pending_scan[j] = ghosts_pending_scan[i]
			j = j + 1
		end

		update_helper(ghosts_to_scan, nil, true)

		ghosts_pending_scan = new_ghosts_pending_scan

		if #new_ghosts_pending_scan == 0 then
			dirty = false
		end
	end
end

function update_network_area(area, network)
	local networks = { network }

	-- Called to update from ghosts in a newly constructed roboport network
	for __, surface in pairs(game.surfaces) do
		-- type(surface) is string
		ghosts = game.surfaces[surface.name].find_entities_filtered {
			name = {"entity-ghost", "tile-ghost"},
			area = area
		}
		-- update immediately
		update_helper(ghosts, networks, true)
	end
	-- do not clear dirty flag
end

function rescan_all()
	for __, surface in pairs(game.surfaces) do
		-- type(surface) is string
		ghosts = game.surfaces[surface.name].find_entities_filtered {
			name = {"entity-ghost", "tile-ghost"}
		}

		for _, ghost in ipairs(ghosts) do
			ghosts_pending_scan[#ghosts_pending_scan + 1] = ghost
		end

		rescan_proxies(surface)

		mark_dirty()
	end
end

function update_helper(ghosts, networks, rescan)
	for k, v in pairs(classes) do
		for q, i in pairs(data[k]) do
			if i.entity.valid then
				v.on_pre_update(i, q)
			end
		end
	end

	if is_nonempty(ghosts) then
		update_ghosts(ghosts, networks, rescan)
	end

	purge_invalid_ghosts()

	for k, v in pairs(classes) do
		for q, i in pairs(data[k]) do
			if i.entity.valid then
				v.on_finalise_update(i, q)
			end
		end
	end
end

function init()
	data = global.uc_data or {}
	for k, v in pairs(classes) do
		data[k] = data[k] or {}
	end
end

function configuration_changed(cfg)
	if cfg.mod_changes then
		local changes = cfg.mod_changes["ConstructionCombinator"]
		if changes then
			init()
		end
	end
end

function mark_dirty()
	dirty = true
end

script.on_init(init)
script.on_load(init)
script.on_configuration_changed(configuration_changed)
script.on_event(defines.events.on_tick, tick)
script.on_event(defines.events.on_built_entity, entity_built)
script.on_event(defines.events.on_robot_built_entity, entity_built_by_robot)
script.on_event(defines.events.on_pre_player_mined_item, on_entity_removed)
script.on_event(defines.events.on_pre_ghost_deconstructed, on_entity_removed)
script.on_event(defines.events.on_robot_pre_mined, on_entity_removed)
script.on_event(defines.events.on_entity_died, on_entity_died)

script.on_event(defines.events.on_robot_built_tile, entity_built_by_robot)
script.on_event(defines.events.on_player_built_tile, entity_built_by_robot)
